# AcubeSAT survey on hybrid and remote work scheme

## Overview

A survey aiming to understand the opinions of AcubeSAT members on the hybrid/remote work that the SpaceDot team has been implementing. It focuses on a few different categories, namely:

1. Introductory questions
2. Issues with remote work
3. Platform ranking
4. Distance & Time spent on the team
5. IRL vs remote/hybrid work schemes

This survey has been prepared as part of the paper titled **"Adaptation of a nanosatellite project into remote working during the COVID-19 era"** by Anastasios-Faidon Retselis, Konstantinos Kanavouras and Theodoros Papafotiou. The paper will be presented on the 4th Symposium for Space Educational Activities in Barcelona, Spain.

## About the survey

| **Information** | **Value** |
|:------------:|:-----:|
|    **Target group population (AcubeSAT members)**   | 79 |
|    **Number of complete responses gathered**    | 58 |
|    **Participation percentage**    | 73.42 |

## Overview of Results

### Did you join AcubeSAT during the COVID-19 pandemic?

| **Response** | **%** |
|:------------:|:-----:|
|    **Yes**   | 79.31 |
|    **No**    | 20.69 |

![alt text](Figures/acubesatjoin.png "acubesatjoin")


### As a student/AcubeSAT member, do you live in Thessaloniki or in close proximity to Thessaloniki?

| **Response** | **%** |
|:------------:|:-----:|
|    **Yes**   | 84.48 |
|    **No**    | 15.52 |

![alt text](Figures/location.png "location")


### How has Productivity changed due to the remote/hybrid work scheme that the team has been implementing?

| **Response** | **%** |
|:------------:|:-----:|
| **Severely Worse**   | 1.72  |
| **Worse**            | 8.62  |
| **No Change**        | 36.21 |
| **Improved**         | 48.28 |
| **Severely Improved**| 5.17  |

![alt text](Figures/problemsrate[SQ001].png "problemsrate[SQ001]")


### How has Cooperation between team members changed due to the remote/hybrid work scheme that the team has been implementing?

| **Response** | **%** |
|:------------:|:-----:|
| **Severely Worse**   | 3.45   |
| **Worse**            | 39.66  |
| **No Change**        | 24.14  |
| **Improved**         | 31.03  |
| **Severely Improved**| 1.72   |

![alt text](Figures/problemsrate[SQ002].png "problemsrate[SQ002]")


### How has Amount of errors/mistakes changed due to the remote/hybrid work scheme that the team has been implementing?

| **Response** | **%** |
|:------------:|:-----:|
| **Severely Worse**   | 1.72   |
| **Worse**            | 13.79  |
| **No Change**        | 67.24  |
| **Improved**         | 17.25  |
| **Severely Improved**| 0.00   |

![alt text](Figures/problemsrate[SQ003].png "problemsrate[SQ003]")


### How has Psychological well-being changed due to the remote/hybrid work scheme that the team has been implementing?

| **Response** | **%** |
|:------------:|:-----:|
| **Severely Worse**   | 24.14  |
| **Worse**            | 46.55  |
| **No Change**        | 20.69  |
| **Improved**         | 6.90   |
| **Severely Improved**| 1.72   |

![alt text](Figures/problemsrate[SQ004].png "problemsrate[SQ004]")


### How has Fun changed due to the remote/hybrid work scheme that the team has been implementing?

| **Response** | **%** |
|:------------:|:-----:|
| **Severely Worse**   | 15.52  |
| **Worse**            | 53.45  |
| **No Change**        | 22.41  |
| **Improved**         | 6.90   |
| **Severely Improved**| 1.72   |

![alt text](Figures/problemsrate[SQ005].png "problemsrate[SQ005]")


### (New Members during on-boarding) I found it easy to ask questions during my training.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 65.22  |
| **Neutral**  | 28.26  |
| **Disagree** | 6.52   |


![alt text](Figures/newmemberintro[SQ001].png "newmemberintro[SQ001]")


### (New Members during on-boarding) There were no technical issues during the remote introduction.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 63.04  |
| **Neutral**  | 17.39  |
| **Disagree** | 19.57  |


![alt text](Figures/newmemberintro[SQ002].png "newmemberintro[SQ002]")


### (New Members during on-boarding) At the time, I felt welcome inside the team.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 71.74  |
| **Neutral**  | 23.91  |
| **Disagree** | 4.35   |


![alt text](Figures/newmemberintro[SQ003].png "newmemberintro[SQ003]")


### (New Members during on-boarding) I got to meet my fellow subsystem members.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 65.22  |
| **Neutral**  | 17.39  |
| **Disagree** | 17.39  |


![alt text](Figures/newmemberintro[SQ004].png "newmemberintro[SQ004]")


### (New Members during on-boarding) I got to meet my fellow team members.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 36.96 |
| **Neutral**  | 36.96  |
| **Disagree** | 26.08  |


![alt text](Figures/newmemberintro[SQ005].png "newmemberintro[SQ005]")


### (New Members during on-boarding) I now feel part of the team.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 76.09 |
| **Neutral**  | 21.74  |
| **Disagree** | 2.17   |


![alt text](Figures/newmemberintro[SQ006].png "newmemberintro[SQ006]")


### (New Members during on-boarding) I feel welcome to join the team's facilities.

| **Response** | **%** |
|:------------:|:-----:|
| **Agree**    | 89.13  |
| **Neutral**  | 6.52   |
| **Disagree** | 4.35   |


![alt text](Figures/newmemberintro[SQ007].png "newmemberintro[SQ007]")


### (Subsystem Work) How would you prefer the following team activities to be done moving forward?

| **Response** | **%** |
|:------------:|:-----:|
| **Totally Remote**          | 1.72   |
| **Hybrid & Mostly Remote**  | 39.66  |
| **Hybrid & Mostly On-site** | 56.90  |
| **Totally On-site**         | 1.72   |


![alt text](Figures/ivrwork[SQ001].png "ivrwork[SQ001]")


### (Subsystem Meetings) How would you prefer the following team activities to be done moving forward?

| **Response** | **%** |
|:------------:|:-----:|
| **Totally Remote**          | 18.97  |
| **Hybrid & Mostly Remote**  | 53.45  |
| **Hybrid & Mostly On-site** | 25.86  |
| **Totally On-site**         | 1.72   |


![alt text](Figures/ivrwork[SQ002].png "ivrwork[SQ002]")


### (AcubeSAT Meetings) How would you prefer the following team activities to be done moving forward?

| **Response** | **%** |
|:------------:|:-----:|
| **Totally Remote**          | 43.11  |
| **Hybrid & Mostly Remote**  | 31.03  |
| **Hybrid & Mostly On-site** | 22.41  |
| **Totally On-site**         | 3.45   |


![alt text](Figures/ivrwork[SQ003].png "ivrwork[SQ003]")


### (Technical Sessions) How would you prefer the following team activities to be done moving forward?

| **Response** | **%** |
|:------------:|:-----:|
| **Totally Remote**          | 1.72   |
| **Hybrid & Mostly Remote**  | 1.72   |
| **Hybrid & Mostly On-site** | 44.83  |
| **Totally On-site**         | 51.73  |


![alt text](Figures/ivrwork[SQ004].png "ivrwork[SQ004]")


### (Fun activities) How would you prefer the following team activities to be done moving forward?

| **Response** | **%** |
|:------------:|:-----:|
| **Totally Remote**          | 6.90   |
| **Hybrid & Mostly Remote**  | 32.76  |
| **Hybrid & Mostly On-site** | 41.38  |
| **Totally On-site**         | 18.96  |


![alt text](Figures/ivrwork[SQ005].png "ivrwork[SQ005]")


### (1-on-1 meetings) How would you prefer the following team activities to be done moving forward?

| **Response** | **%** |
|:------------:|:-----:|
| **Totally Remote**          | 13.79  |
| **Hybrid & Mostly Remote**  | 36.21  |
| **Hybrid & Mostly On-site** | 39.66  |
| **Totally On-site**         | 10.34  |


![alt text](Figures/ivrwork[SQ006].png "ivrwork[SQ006]")


### How much time does it take for you to reach the team's lab from your home?

| **Response** | **%** |
|:------------:|:-----:|
| **0-15 minutes**          | 34.69  |
| **15-30 minutes**         | 36.73  |
| **30-45 minutes**         | 18.37  |
| **45+ minutes**           | 10.21  |


![alt text](Figures/timetravelling.png "timetravelling")


### How many kilometers away from the team's lab do you live?

| **Response** | **%** |
|:------------:|:-----:|
| **0-0.5 km**        | 0.00   |
| **0.5-1 km**        | 27.66  |
| **1-2 km**          | 27.66  |
| **2-3 km**          | 0.00   |
| **3-4 km**          | 17.02  |
| **4-5 km**          | 6.38   |
| **5+ km**           | 21.28  |


![alt text](Figures/distancelab.png "distancelab")


### What percentage of your daily time (ignoring bed time) do you spend for AcubeSAT?

| **Response** | **%** |
|:------------:|:-----:|
| **0-20%**          | 39.66  |
| **20-40%**         | 39.66  |
| **40-60%**         | 13.79  |
| **60-80%**         | 6.89   |
| **80-100%**        | 0.00   |


![alt text](Figures/timeacubesat.png "timeacubesat")


### What percentage of your AcubeSAT time do you spend in the lab?

| **Response** | **%** |
|:------------:|:-----:|
| **0-20%**          | 55.11  |
| **20-40%**         | 10.20  |
| **40-60%**         | 12.24  |
| **60-80%**         | 14.29  |
| **80-100%**        | 8.16   |


![alt text](Figures/timelab.png "timelab")


### What percentage of your AcubeSAT time do you spend in internal meetings?

| **Response** | **%** |
|:------------:|:-----:|
| **0-20%**          | 41.38  |
| **20-40%**         | 27.59  |
| **40-60%**         | 12.07  |
| **60-80%**         | 10.34  |
| **80-100%**        | 8.62   |


![alt text](Figures/timemeetings.png "timemeetings")


### What percentage of your AcubeSAT time do you spend on the road using your mobile phone?

| **Response** | **%** |
|:------------:|:-----:|
| **0-20%**          | 81.03  |
| **20-40%**         | 12.07  |
| **40-60%**         | 0.00   |
| **60-80%**         | 3.45   |
| **80-100%**        | 3.45   |


![alt text](Figures/timeoutside.png "timeoutside")